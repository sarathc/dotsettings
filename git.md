# Git customizations
## Completion
https://github.com/bobthecow/git-flow-completion/wiki/Install-Bash-git-completion

## bash prompt
https://coderwall.com/p/fasnya/add-git-branch-name-to-bash-prompt

```bash
parse_git_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}
export PS1="\u@local \[\033[32m\]\w\[\033[33m\]\$(parse_git_branch)\[\033[00m\] $ "
```

## Aliases
add this to ~/.git_config
```ini
[alias]
        # ones I commonly use
        # create a fork on UI first eg: platform/terraform -> sarath.chandra/terraform
        # start with clone - 
        # git new terraform
        new = !sh -c 'git clone git@git.wmedia.tech:sarath.chandra/$1.git' - 
        # set up upstream repo  
        # git upstream platform/terraform
        upstream = !sh -c 'git remote add upstream git@git.wmedia.tech:$1.git' - 
        # create new feature, always start with a new branch from upstream. 
        # note: if you have local changes, you should stash working copy or checkin to working branch first
        # git feat some-feature-name
        feat = !sh -c 'git fetch upstream master && git checkout FETCH_HEAD -b $1' -
        #rebase on new commit in upstream, run this frequently. (new pushes may require -f)
        rbm = !git fetch upstream master && git rebase FETCH_HEAD
        #look at mr locally for review
        mr = !sh -c 'git fetch upstream merge-requests/$1/head && git checkout FETCH_HEAD' -
        #checkout at mr locally as branch (when you want to work on someone's MR)
        mrb = !sh -c 'git fetch upstream merge-requests/$1/head:mr-$1 && git checkout mr-$1' -
        al = !sh -c 'git config --global -l | grep alias' -
        configedit = !sh -c 'code ~/.gitconfig'
        ce = !sh -c 'code ~/.gitconfig'
        ra = remote -v
        l = log -4
        lt = log --graph --all
        lg = log --graph --abbrev-commit --decorate --date=relative --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)' --all
        ls = status
        remote-url = !git remote get-url ${1:-origin} | sed 's/^git@\\(.*\\):\\(.*\\).git/https:\\/\\/\\1\\/\\2/'
        ru = !git remote get-url ${1:-origin} | sed 's/^git@\\(.*\\):\\(.*\\).git/https:\\/\\/\\1\\/\\2/'
        fP = fetch -p -P -t
        ft = fetch -t      
        f = fetch     
        rh = reset HEAD
        r1 = reset HEAD~1 #undo last commit
        sqm = !sh -c 'git co -b $1-sq master && git merge $1 --squash' - # no longer required - gitlb mr can squash
        b = branch -v
        ba = branch -av
        bd = branch -d
        bD = branch -D
        ci = commit
        co = checkout
        cb = checkout -b
        tl = tag --list
        #Other useful
        d = diff
        dc = diff --cached
        fp = fetch -p
        g = !git gui &
        gr = log --graph
        g1 = log --graph --pretty=oneline --abbrev-commit
        k = !gitk &
        ka = !gitk --all &
        lc = log ORIG_HEAD.. --stat --no-merges
        lp = log --patch-with-stat
        mnf = merge --no-ff
        mnff = merge --no-ff
        mt = mergetool
        p = format-patch -1
        serve = !git daemon --reuseaddr --verbose  --base-path=. --export-all ./.git
        sh = !git-sh
        st = status
        stm = status --untracked=no
        stfu = status --untracked=no
```