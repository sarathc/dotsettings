# dotsettings - settings I use for local development

a general list of shortcuts, files, and configuration recommendations for a developer

## tools

* brew
* [vscode](vscode.md)
* [git](git.md)
* [aws](aws.md)
* [docker](docker.md)

