# VS Code
## Extenstions

### My list
```bash
code --list-extensions

2gua.rainbow-brackets
k--kato.intellij-idea-keybindings
mauve.terraform
ms-azuretools.vscode-docker
ms-python.python
ms-vscode-remote.vscode-remote-extensionpack
ritwickdey.LiveServer
VisualStudioExptTeam.vscodeintellicode

```

### Installation

```bash

EXT_LIST="2gua.rainbow-brackets
k--kato.intellij-idea-keybindings
mauve.terraform
ms-azuretools.vscode-docker
ms-python.python
ms-vscode-remote.vscode-remote-extensionpack
ritwickdey.LiveServer
VisualStudioExptTeam.vscodeintellicode"

IFS=$(echo -en "\n\b") && read -ra EXTN_I <<< "$EXT_LIST" # str is read into an array as tokens separated by IFS
for i in "${EXTN_I[@]}"; do # access each element of array
    echo "code --install-extension $i"
done
IFS=' ' # reset to default value after usage

```